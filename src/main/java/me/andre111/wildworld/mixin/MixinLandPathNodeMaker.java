/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import me.andre111.wildworld.WildWorld;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.ai.pathing.LandPathNodeMaker;
import net.minecraft.entity.ai.pathing.PathNodeType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;

@Mixin(LandPathNodeMaker.class)
public class MixinLandPathNodeMaker {
	// add a case (using mixin) to LandPathNodeMaker.getCommonNodeType to let thorns blocks return type DAMAGE_OTHER to make mobs avoid walking through spikes
	@Inject(method = "getCommonNodeType", at = @At("HEAD"), cancellable = true)
	private static void onGetCommonNodeType(BlockView blockView, BlockPos blockPos, CallbackInfoReturnable<PathNodeType> cir) {
		BlockState blockState = blockView.getBlockState(blockPos);
		Block block = blockState.getBlock();
		if(block == WildWorld.NETHER_THORNS || block == WildWorld.END_THORNS) {
			cir.setReturnValue(PathNodeType.DAMAGE_OTHER);
		}
	}
}
