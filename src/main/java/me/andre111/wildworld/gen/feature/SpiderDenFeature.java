/*
 * Copyright (c) 2020 André Schweiger
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package me.andre111.wildworld.gen.feature;

import java.util.Random;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.mojang.serialization.Codec;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.MobSpawnerBlockEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.Heightmap;
import net.minecraft.world.StructureWorldAccess;
import net.minecraft.world.gen.chunk.ChunkGenerator;
import net.minecraft.world.gen.feature.DefaultFeatureConfig;
import net.minecraft.world.gen.feature.Feature;

public class SpiderDenFeature extends Feature<DefaultFeatureConfig> {
	private static final Logger LOGGER = LogManager.getLogger();
	private static final BlockState COBWEB;
	
	public SpiderDenFeature(Codec<DefaultFeatureConfig> codec) {
		super(codec);
	}

	@Override
	public boolean generate(StructureWorldAccess world, ChunkGenerator generator, Random random, BlockPos pos, DefaultFeatureConfig config) {
		if (world.isAir(pos) && pos.getY() < 40 && pos.getY() < world.getTopPosition(Heightmap.Type.MOTION_BLOCKING, pos).getY() / 2) {
			// create spawner
			world.setBlockState(pos, Blocks.SPAWNER.getDefaultState(), 2);
			BlockEntity blockEntity_1 = world.getBlockEntity(pos);
			if (blockEntity_1 instanceof MobSpawnerBlockEntity) {
				((MobSpawnerBlockEntity) blockEntity_1).getLogic().setEntityId(EntityType.SPIDER);
			} else {
				LOGGER.error("Failed to fetch mob spawner entity at ({}, {}, {})", pos.getX(), pos.getY(), pos.getZ());
			}

			// create random cobwebs
			BlockPos.Mutable mpos = pos.mutableCopy();
			for(int i = 0; i < 64; ++i) {
				mpos.set(pos);
				mpos.move(random.nextInt(8) - random.nextInt(8), random.nextInt(4) - random.nextInt(4), random.nextInt(8) - random.nextInt(8));
				
				if (world.isAir(mpos) && mpos.getY() < 255 && COBWEB.canPlaceAt(world, mpos)) {
					world.setBlockState(mpos, COBWEB, 2);
				}
			}

			return true;
		}
		
		return false;
	}

	static {
		COBWEB = Blocks.COBWEB.getDefaultState();
	}
}
